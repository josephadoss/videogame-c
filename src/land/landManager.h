//#include "../globals.h"

//#include "land.h"

//#include "plots/plot.h"
#include "panels/panel.h"


class landManager
{
	public:
		landManager( );

		void render( );

		void stepUp( );
		void stepDown( );
		void stepLeft( );
		void stepRight( );

	protected:

	private:
		panel panels[PANELS_ARRAY_MAX][PANELS_ARRAY_MAX];
		//plot plots[2][2];
};
