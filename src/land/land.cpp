#include "land.h"

land::land( )
{
	std::cout << "land( )\n";
	colorBlue = (float)rand( )/(float)RAND_MAX; 
	colorGreen = (float)rand( )/(float)RAND_MAX;
	colorRed = (float)rand( )/(float)RAND_MAX;
}

land::land( int row , int col )
{
	std::cout << "land( " << row << " , " << col << " )\n";

	maxX =  .5f;
	minX = -.5f;
	maxY =  .5f;
	minY = -.5f;


	switch( col )
	{
		case 0 :
			minX -= 2.0;
			maxX -= 2.0;
			break ;
		case 1 :
			minX -= 1.0;
			maxX -= 1.0;
			break;
		case 2 :
			minX = -.5;
			maxX = .5;
			break;
		case 3 :
			minX += 1.0;
			maxX += 1.0;
			break;
		case 4 :
			minX += 2.0;
			maxX += 2.0;
			break;
		default :
			break;
	}

	switch( row )
	{
		case 0 :
			minY += 2.0;
			maxY += 2.0;
			break ;
		case 1 :
			minY += 1.0;
			maxY += 1.0;
			break;
		case 2 :
			minY = -.5;
			maxY = .5;
			break;
		case 3 :
			minY -= 1.0;
			maxY -= 1.0;
			break;
		case 4 :
			minY -= 2.0;
			maxY -= 2.0;
			break;
		default :
			break;
	}

	colorBlue = (float)rand( )/(float)RAND_MAX; 
	colorGreen = (float)rand( )/(float)RAND_MAX;
	colorRed = (float)rand( )/(float)RAND_MAX;
}


float land::getMinY( )
{
	return minY;
}

float land::getMaxY( )
{
	return maxY;
}

void land::moveUp( )
{
	minY += 1.0;
	maxY += 1.0;
}

void land::moveDown( )
{
	std::cout << "land::moveDown( )\n";

	minY -= 1.0;
	maxY -= 1.0;
}

void land::moveLeft( )
{
	minX -= 1.0;
	maxX -= 1.0;
}

void land::moveRight( )
{
	minX += 1.0;
	maxX += 1.0;
}

void land::render( )
{
	glBegin(GL_QUADS);
		glColor3f( colorRed , colorGreen , colorBlue );

                glVertex3f(minX,maxY,0.0f);
                glVertex3f(minX,minY,0.0f);
                glVertex3f(maxX,minY,0.0f);
		glVertex3f(maxX,maxY,0.0f);

	glEnd();
}

void land::stepUp( )
{
	minY += STEP;
	maxY += STEP;
}
