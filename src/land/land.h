#include "../globals.h"
#include <iostream>

#include <stdio.h>
#include <stdlib.h>

#include <GL/gl.h>
#include <GL/glu.h>

class land
{
	public:
		void render( );
		land( );
		land( int , int );

		int getXLoc( );
		int getYLoc( );

		float getMinX( );
		float getMaxX( );
		float getMinY( );
		float getMaxY( );

		void moveUp( );
		void moveDown( );
		void moveLeft( );
		void moveRight( );

		void increaseX( float );
		void decreaseX( float );
		void increaseY( float );
		void decreaseY( float );
		void increaseZ( float );
		void decreaseZ( float );

		void stepUp( );
		void stepDown( );
		void stepLeft( );
		void stepRight( );

	protected:
	private:
		float maxX;// = .5f;
		float minX;// = -.5f;
		float maxY;// = .5f;
		float minY;// = -.5f;

		float colorRed;
		float colorBlue;
		float colorGreen;
		
};
