#include "../../globals.h"
#include <iostream>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <GL/gl.h>
#include <GL/glu.h>


// square foot
// two triangles 
class panel
{
	public:
		panel( );
		panel( float , float , float );

		void render( );

		void setCenter( float , float , float );

		void stepUp( );
		void stepDown( );
		void stepLeft( );
		void stepRight( );

	protected:

	private:
		float colorRed;
		float colorBlue;
		float colorGreen;

		float centerX;
		float centerY;
		float centerZ;
};



