#include "panel.h"



panel::panel( ) 
{ 	
	srand(clock( ));
	colorBlue = (float)rand( )/(float)RAND_MAX; 
	colorGreen = (float)rand( )/(float)RAND_MAX;
	colorRed = (float)rand( )/(float)RAND_MAX; 

	std::cout << "blue : " << colorBlue ;
};

panel::panel( float x , float y , float z )
{
	panel( );
	setCenter( x , y , z );
};

void panel::render( ) 
{	
	float halfStep = 0.25;//STEP / 2.0 ; 

	glBegin(GL_QUADS);
		//glColor3f( colorRed , colorGreen , colorBlue );
		glColor3f( 1.0f , 1.0f , 1.0f );

//                glVertex3f( centerX - halfStep , centerY + halfStep , centerZ );
//	        glVertex3f( centerX - halfStep , centerY - halfStep , centerZ );
//                glVertex3f( centerX + halfStep , centerY - halfStep , centerZ );
//		glVertex3f( centerX + halfStep , centerY + halfStep , centerZ );

                glVertex3f( centerX - halfStep , centerY + halfStep , centerZ );
		glVertex3f( centerX + halfStep , centerY + halfStep , centerZ );
                glVertex3f( centerX + halfStep , centerY - halfStep , centerZ );
	        glVertex3f( centerX - halfStep , centerY - halfStep , centerZ );

	glEnd();
};

void panel::setCenter( float x , float y , float z )
{
	centerX = x;
	centerY = y;
	centerZ = z;
}


void panel::stepUp( )
{
	centerY -= STEP;
}

void panel::stepDown( )
{
	centerY += STEP;
}

void panel::stepLeft( )
{
	centerX += STEP;
}

void panel::stepRight( )
{
	centerX -= STEP;
}








