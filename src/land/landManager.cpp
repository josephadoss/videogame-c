#include "landManager.h"



landManager::landManager( )
{

	std::cout << "landManager( )\n";

	for( int row = 0 ; row < PANELS_ARRAY_MAX ; row++ )
	{
		for( int col = 0 ; col < PANELS_ARRAY_MAX ; col++ )
		{
			panels[row][col] =  panel( ( float )row  , ( float )col , 0.0 );
		}
	}
}


void landManager::render( )
{
	for( int row = 0 ; row < PANELS_ARRAY_MAX ; row++ )
	{
		for( int col = 0 ; col < PANELS_ARRAY_MAX ; col++ )
		{
			panels[row][col].render( );
		}
	}
}


void landManager::stepUp( )
{
	for( int row = 0 ; row < PANELS_ARRAY_MAX ; row++ )
	{
		for( int col = 0 ; col < PANELS_ARRAY_MAX ; col++ )
		{
			panels[row][col].stepUp( );
		}
	}
}


void landManager::stepDown( )
{
	for( int row = 0 ; row < PANELS_ARRAY_MAX ; row++ )
	{
		for( int col = 0 ; col < PANELS_ARRAY_MAX ; col++ )
		{
			panels[row][col].stepDown( );
		}
	}
}


void landManager::stepLeft( )
{
	for( int row = 0 ; row < PANELS_ARRAY_MAX ; row++ )
	{
		for( int col = 0 ; col < PANELS_ARRAY_MAX ; col++ )
		{
			panels[row][col].stepLeft( );
		}
	}
}


void landManager::stepRight( )
{
	for( int row = 0 ; row < PANELS_ARRAY_MAX ; row++ )
	{
		for( int col = 0 ; col < PANELS_ARRAY_MAX ; col++ )
		{
			panels[row][col].stepRight( );
		}
	}
}



/*
landManager::landManager( )
{

	std::cout << "landManager( )\n";

	for( int row = 0 ; row < 2 ; row++ )
	{
		for( int col = 0 ; col < 2 ; col++ )
		{
			//plots[row][col] =  plot( row , col );
		}
	}
}


void landManager::render( )
{
	for( int row = 0 ; row < 2 ; row++ )
	{
		for( int col = 0 ; col < 2 ; col++ )
		{
			//plots[row][col].render( );
		}
	}
}


void landManager::stepUp( )
{
	for( int row = 0 ; row < 2 ; row++ )
	{
		for( int col = 0 ; col < 2 ; col++ )
		{
			//plots[row][col].stepUp( );
		}
	}
}


void landManager::stepDown( )
{
	for( int row = 0 ; row < 2 ; row++ )
	{
		for( int col = 0 ; col < 2 ; col++ )
		{
			//plots[row][col].stepDown( );
		}
	}
}


void landManager::stepLeft( )
{
	for( int row = 0 ; row < 2 ; row++ )
	{
		for( int col = 0 ; col < 2 ; col++ )
		{
			//plots[row][col].stepLeft( );
		}
	}
}


void landManager::stepRight( )
{
	for( int row = 0 ; row < 2 ; row++ )
	{
		for( int col = 0 ; col < 2 ; col++ )
		{
			//plots[row][col].stepRight( );
		}
	}
}

*/
