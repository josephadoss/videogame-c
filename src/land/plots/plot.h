#include "../../globals.h"
#include <iostream>

#include <stdio.h>
#include <stdlib.h>

#include <GL/gl.h>
#include <GL/glu.h>

#include "../panels/panel.h"

// 5280^2 panels
// a square mile
class plot
{
	public:
		plot( );
		plot( int , int );

		void render( );

		void stepUp( );
		void stepDown( );
		void stepLeft( );
		void stepRight( );
		

	protected:

	private:
		//panel panels[4][4];
		panel panels[PANELS_ARRAY_MAX][PANELS_ARRAY_MAX];
		//panel panels[5280][5280];
		//panel panels[][];
};
