#include "enemy.h"


enemy::enemy( )
{
	locX = 0.0f;
	locY = 0.0f;
	locZ = 0.0f;
	speed = 0.02*FOOT;//2.0 * FOOT;
	height = 2.0 * FOOT;
	cout << "enemy\n";
	std::cout << "height : " << height << "\n";
	std::cout << "speed : " << speed << "\n";
	setHealth( 10 );
	alive = true;

}

enemy::enemy( float x , float y , float z )
{
	enemy( );
	locX = x;
	locY = y;
	locZ = z;
}

void enemy::attack( )
{
	float x = locX;
	float y = locY;

	// seek out the main player and his animals
	// if within range
		//attack
	// else
	
}

void enemy::attacked( int damage )
{
	setHealth( getHealth( ) - damage );
	cout << "just attacked, health now " << getHealth( ) << "\n";

	if( isAlive( ) < 1 )
	{
		die( );
	}
}

void enemy::die( )
{
	alive = false;
}

void enemy::faceNorth( )
{
	facingDirection = "N";
}

void enemy::faceSouth( )
{
	facingDirection = "S";
}

void enemy::faceEast( )
{
	facingDirection = "E";
}

void enemy::faceWest( )
{
	facingDirection = "W";
}

void enemy::faceNorthEast( )
{
	facingDirection = "NE";
}

void enemy::faceNorthWest( )
{
	facingDirection = "NW";
}

void enemy::faceSouthEast( )
{
	facingDirection = "SE";
}

void enemy::faceSouthWest( )
{
	facingDirection = "SW";
}

float enemy::getX( )
{
	return locX;
}

float enemy::getY( )
{
	return locY;
}

float enemy::getZ( )
{
	return locZ ;
}

bool enemy::isAlive( )
{
	if( getHealth( ) > 0 )
	{
		return true;
	}

	return false;
}


void enemy::offset( float x , float y , float z )
{
	offsetX = x;
	offsetY = y;
	offsetZ = z;

	//std::cout << "enemy :   locX : " << locX << " locY : " << locY << " locZ : " << locZ << "\n";
}

void enemy::render( )
{
	//glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	//glLoadIdentity( );
	float centerX = locX ;//- offsetX;
	float centerY = locY ;//- offsetY;
	float centerZ = locZ ;//- offsetZ;

	//cout << "enemy.render  : centerX " << centerX << " centerY " << centerY << " centerZ " << centerZ << "\n";

	float halfStep = 0.5;
	glBegin(GL_QUADS);
		//glColor3f( colorRed , colorGreen , colorBlue );
		glColor3f( 1.0f , 0.0f , 0.0f );

//                glVertex3f( centerX - halfStep , centerY + halfStep , centerZ );
//	        glVertex3f( centerX - halfStep , centerY - halfStep , centerZ );
//                glVertex3f( centerX + halfStep , centerY - halfStep , centerZ );
//		glVertex3f( centerX + halfStep , centerY + halfStep , centerZ );

                glVertex3f( centerX - offsetX - halfStep , centerY - offsetY + halfStep , offsetZ + centerZ );
		glVertex3f( centerX - offsetX + halfStep , centerY - offsetY + halfStep , offsetZ + centerZ );
                glVertex3f( centerX - offsetX + halfStep , centerY - offsetY - halfStep , offsetZ + centerZ );
	        glVertex3f( centerX - offsetX - halfStep , centerY - offsetY - halfStep , offsetZ + centerZ );

	glEnd();

/*
	glBegin( GL_TRIANGLES );
		glColor3f( 1.0f, 0.0f, 0.0f );
		
		glVertex3f( centerX-.02  , centerY-.02 , centerZ );
		glVertex3f( centerX+.02  , centerY-.02 , centerZ );
		glVertex3f( centerX      , centerY+.02 , centerZ );

	glEnd( );

	glBegin( GL_TRIANGLES );
		glColor3f( 1.0f, 0.0f, 0.0f );
		glVertex3f( centerX-.02  , centerY-.02 , centerZ );
		glVertex3f( centerX+.02  , centerY-.02 , centerZ );
		glVertex3f( centerX      , centerY     , centerZ+height );


	glEnd( );

	glBegin( GL_TRIANGLES );
		glColor3f( 1.0f, 0.0f, 0.0f );
		
		glVertex3f( centerX     , centerY+.02 , centerZ );
		glVertex3f( centerX-.02 , centerY-.02 , centerZ );
		glVertex3f( centerX     , centerY     , centerZ+height );

	glEnd( );

	glBegin( GL_TRIANGLES );
		glColor3f( 1.0f, 0.0f, 0.0f );
		
		glVertex3f( centerX     , centerY+.02 , centerZ );
		glVertex3f( centerX+.02 , centerY-.02 , centerZ );
		glVertex3f( centerX     , centerY     , centerZ+height );

	glEnd( );
*/


}

void enemy::setHealth( int x )
{
	health = x;
}

