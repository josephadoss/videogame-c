#ifndef CHARACTER_H
#define CHARACTER_H


#include "globals.h"
#include "fightable.h"
#include "enemy.h"

//#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
//#include <GL/glut.h>
//#include <GL/glx.h>

//#include <GL/freeglut.h>
//#include <X11/Xlib.h>
//#include <X11/Xutil.h>


#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <unistd.h>

//#include <X11/Xlib.h>

using namespace std;

class character : private fightable
{
	public:
		character( );

		void faceNorth( );
		void faceSouth( );
		void faceEast( );
		void faceWest( );
		void faceNorthEast( );
		void faceNorthWest( );
		void faceSouthEast( );
		void faceSouthWest( );

		void offset( float , float , float );
		void render( );
		float getX( );
		float getY( );
		float getZ( );

		void attack( );
		void attacked( int );

		void setEnemies( vector<enemy>* );
	
	protected:
	private:
		float locX;
		float locY;
		float locZ;
		float speed;
		float height;

		float offsetX;
		float offsetY;
		float offsetZ;

		string facingDirection;
	
		

		vector<enemy>* enemies;

};


#endif
