#ifndef LANDMANAGER_H
#define LANDMANAGER_H

#include "globals.h"


//#include "land.h"

//#include "plots/plot.h"
#include "land/panels/panel.h"
#include "enemy.h"

using namespace std;

class landManager
{
	public:
		landManager( );

		void offset( float , float , float );

		void render( );

		void stepUp( );
		void stepDown( );
		void stepLeft( );
		void stepRight( );

		vector<enemy>* getEnemies( );


	protected:

	private:
		panel panels[PANELS_ARRAY_MAX][PANELS_ARRAY_MAX];
		//plot plots[2][2];

		float offsetX;
		float offsetY;
		float offsetZ;

		vector<enemy>* enemies;
};


#endif
