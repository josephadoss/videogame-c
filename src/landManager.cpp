#include "landManager.h"



landManager::landManager( )
{

	std::cout << "landManager( )\n";

	for( int row = 0 ; row < PANELS_ARRAY_MAX ; row++ )
	{
		for( int col = 0 ; col < PANELS_ARRAY_MAX ; col++ )
		{
			panels[row][col] =  panel( ( float )row  , ( float )col , 0.0 );
		}
	}

	enemies = new vector<enemy>;

	enemies->push_back( enemy( -2.0 , -3.0 , 0.0 ) );
	enemies->push_back( enemy( 5.0 , 13.0 , 0.0 ) );
	enemies->push_back( enemy( 8.0 , 23.0 , 0.0 ) );
	enemies->push_back( enemy( 29.0 , 33.0 , 0.0 ) );
	enemies->push_back( enemy( 44.0 , 33.0 , 0.0 ) );

	
}

vector<enemy>* landManager::getEnemies( )
{
	return enemies;
}

void landManager::offset( float x , float y , float z )
{
	offsetX = x;
	offsetY = y;
	offsetZ = z;

	for( vector<enemy>::iterator i = enemies->begin( ) ; i != enemies->end( ) ; i++ )
	{
		i->offset( x , y , z );
	}
}


void landManager::render( )
{
	for( int row = 0 ; row < PANELS_ARRAY_MAX ; row++ )
	{
		for( int col = 0 ; col < PANELS_ARRAY_MAX ; col++ )
		{
			panels[row][col].offset( offsetX , offsetY , offsetZ );
			panels[row][col].render( );
		}
	}
}


void landManager::stepUp( )
{
	for( int row = 0 ; row < PANELS_ARRAY_MAX ; row++ )
	{
		for( int col = 0 ; col < PANELS_ARRAY_MAX ; col++ )
		{
			panels[row][col].stepUp( );
		}
	}
}


void landManager::stepDown( )
{
	for( int row = 0 ; row < PANELS_ARRAY_MAX ; row++ )
	{
		for( int col = 0 ; col < PANELS_ARRAY_MAX ; col++ )
		{
			panels[row][col].stepDown( );
		}
	}
}


void landManager::stepLeft( )
{
	for( int row = 0 ; row < PANELS_ARRAY_MAX ; row++ )
	{
		for( int col = 0 ; col < PANELS_ARRAY_MAX ; col++ )
		{
			panels[row][col].stepLeft( );
		}
	}
}


void landManager::stepRight( )
{
	for( int row = 0 ; row < PANELS_ARRAY_MAX ; row++ )
	{
		for( int col = 0 ; col < PANELS_ARRAY_MAX ; col++ )
		{
			panels[row][col].stepRight( );
		}
	}
}



/*
landManager::landManager( )
{

	std::cout << "landManager( )\n";

	for( int row = 0 ; row < 2 ; row++ )
	{
		for( int col = 0 ; col < 2 ; col++ )
		{
			//plots[row][col] =  plot( row , col );
		}
	}
}


void landManager::render( )
{
	for( int row = 0 ; row < 2 ; row++ )
	{
		for( int col = 0 ; col < 2 ; col++ )
		{
			//plots[row][col].render( );
		}
	}
}


void landManager::stepUp( )
{
	for( int row = 0 ; row < 2 ; row++ )
	{
		for( int col = 0 ; col < 2 ; col++ )
		{
			//plots[row][col].stepUp( );
		}
	}
}


void landManager::stepDown( )
{
	for( int row = 0 ; row < 2 ; row++ )
	{
		for( int col = 0 ; col < 2 ; col++ )
		{
			//plots[row][col].stepDown( );
		}
	}
}


void landManager::stepLeft( )
{
	for( int row = 0 ; row < 2 ; row++ )
	{
		for( int col = 0 ; col < 2 ; col++ )
		{
			//plots[row][col].stepLeft( );
		}
	}
}


void landManager::stepRight( )
{
	for( int row = 0 ; row < 2 ; row++ )
	{
		for( int col = 0 ; col < 2 ; col++ )
		{
			//plots[row][col].stepRight( );
		}
	}
}

*/
