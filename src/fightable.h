#ifndef FIGHTABLE_H
#define FIGHTABLE_H


class fightable
{
	public:
		fightable( );
		virtual void attack( ) { };
		virtual void attacked( int ) { };
		virtual float getX( ) { }; 
		virtual float getY( ) { };
		virtual float getZ( ) { };

		virtual void faceNorth( ) { };
		virtual void faceSouth( ) { };
		virtual void faceEast( ) { };
		virtual void faceWest( ) { };
		virtual void faceNorthEast( ) { };
		virtual void faceNorthWest( ) { };
		virtual void faceSouthEast( ) { };
		virtual void faceSouthWest( ) { };

		//virtual int getHealth( );

		virtual int getHealth( ) { } ;
		virtual void setHealth( int ) { };
		virtual bool isAlive( ) {};
		virtual void die( ) {};


	protected:

	private:


		int attackDistance;
		int attackDamage;
		int health;
		bool alive;

};

#endif
