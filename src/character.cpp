#include "character.h"

character::character( )
{
	locX = 0.0f;
	locY = 0.0f;
	locZ = 0.0f;
	speed = 0.02*FOOT;//2.0 * FOOT;
	height = 6.0 * FOOT;
	cout << "character( )\n";
	std::cout << "height : " << height << "\n";
	std::cout << "speed : " << speed << "\n";
	attackDistance = 3.0;
	attackDamage = 2;
	alive = true;

}

void character::attack( )
{
	float x = locX;
	float y = locY;

	cout << "player attacking\n";
	
	//get list of enemies
	// if any are nearby, deal them damage with enemy.attack( attackPoints )	
	// else, go through the motions
	for( vector<enemy>::iterator i = enemies->begin( ) ; i != enemies->end( ) ; i++ )
	{
		if( sqrt( pow( ( i->getX( ) - locX ) , 2  ) + pow( ( i->getY( ) - locY ) , 2  ) + pow( ( i->getZ( ) - locZ ) , 2  ) ) <= attackDistance )
		{
			i->attacked( attackDamage );
		}
	}
}

void character::attacked( int damage )
{
	health -= damage;
}

void character::faceNorth( )
{
	facingDirection = "N";
}

void character::faceSouth( )
{
	facingDirection = "S";
}

void character::faceEast( )
{
	facingDirection = "E";
}

void character::faceWest( )
{
	facingDirection = "W";
}

void character::faceNorthEast( )
{
	facingDirection = "NE";
}

void character::faceNorthWest( )
{
	facingDirection = "NW";
}

void character::faceSouthEast( )
{
	facingDirection = "SE";
}

void character::faceSouthWest( )
{
	facingDirection = "SW";
}

float character::getX( )
{
	return locX;
}

float character::getY( )
{
	return locY;
}

float character::getZ( )
{
	return locZ ;
}
/*
void character::moveUp( )
{
	locY += speed;
}

void character::moveUpRight( )
{
	locY += speed;
	locX += speed;
}

void character::moveUpLeft( )
{
	locY += speed;
	locX -= speed;
}

void character::moveDown( )
{
	locY -= speed;
}

void character::moveDownLeft( )
{
	locY -= speed;
	locX -= speed;	
}

void character::moveDownRight( )
{
	locY -= speed;
	locX += speed;
}

void character::moveLeft( )
{
	locX -= speed;
}


void character::moveRight( )
{
	locX += speed;
}
*/

void character::offset( float x , float y , float z )
{
	locX = x;
	locY = y;
	locZ = z;

	//std::cout << "character :   locX : " << x << " locY : " << y << " locZ : " << z << "\n";
}

void character::render( )
{
	//glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	//glLoadIdentity( );
	float centerX = 0.0;
	float centerY = 0.0;
	float centerZ = 0.0;

	glBegin( GL_TRIANGLES );
		glColor3f( 0.0f, 1.0f, 0.0f );
		
		glVertex3f( centerX-.02  , centerY-.02 , centerZ );
		glVertex3f( centerX+.02  , centerY-.02 , centerZ );
		glVertex3f( centerX  , centerY+.02 , centerZ );

	glEnd( );

	glBegin( GL_TRIANGLES );
		glColor3f( 1.0f, 1.0f, 1.0f );
		glVertex3f( centerX-.02  , centerY-.02 , centerZ );
		glVertex3f( centerX+.02  , centerY-.02 , centerZ );
		glVertex3f( centerX  , centerY , centerZ+height );


	glEnd( );

	glBegin( GL_TRIANGLES );
		glColor3f( 1.0f, 0.0f, 0.0f );
		
		glVertex3f( centerX  , centerY+.02 , centerZ );
		glVertex3f( centerX-.02  , centerY-.02 , centerZ );
		glVertex3f( centerX  , centerY , centerZ+height );

	glEnd( );

	glBegin( GL_TRIANGLES );
		glColor3f( 0.0f, 0.0f, 1.0f );
		
		glVertex3f( centerX  , centerY+.02 , centerZ );
		glVertex3f( centerX+.02  , centerY-.02 , centerZ );
		glVertex3f( centerX  , centerY , centerZ+height );

	glEnd( );



}


void character::setEnemies( vector<enemy>* e )
{
	enemies = e;
}

