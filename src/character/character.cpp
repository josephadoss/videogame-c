#include "character.h"

character::character( )
{
	locX = 0.0f;
	locY = 0.0f;
	locZ = 0.0f;
	speed = 0.02*FOOT;//2.0 * FOOT;
	height = 6.0 * FOOT;

	std::cout << "height : " << height << "\n";
	std::cout << "speed : " << speed << "\n";

}

float character::getX( )
{
	return locX;
}

float character::getY( )
{
	return locY;
}

float character::getZ( )
{
	return locZ ;
}
/*
void character::moveUp( )
{
	locY += speed;
}

void character::moveUpRight( )
{
	locY += speed;
	locX += speed;
}

void character::moveUpLeft( )
{
	locY += speed;
	locX -= speed;
}

void character::moveDown( )
{
	locY -= speed;
}

void character::moveDownLeft( )
{
	locY -= speed;
	locX -= speed;	
}

void character::moveDownRight( )
{
	locY -= speed;
	locX += speed;
}

void character::moveLeft( )
{
	locX -= speed;
}


void character::moveRight( )
{
	locX += speed;
}
*/

void character::render( )
{
	//glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	//glLoadIdentity( );

	glBegin( GL_TRIANGLES );
		glColor3f( 0.0f, 1.0f, 0.0f );
		
		glVertex3f( locX-.02  , locY-.02 , locZ );
		glVertex3f( locX+.02  , locY-.02 , locZ );
		glVertex3f( locX  , locY+.02 , locZ );

	glEnd( );

	glBegin( GL_TRIANGLES );
		glColor3f( 1.0f, 1.0f, 1.0f );
		glVertex3f( locX-.02  , locY-.02 , locZ );
		glVertex3f( locX+.02  , locY-.02 , locZ );
		glVertex3f( locX  , locY , locZ+height );


	glEnd( );

	glBegin( GL_TRIANGLES );
		glColor3f( 1.0f, 0.0f, 0.0f );
		
		glVertex3f( locX  , locY+.02 , locZ );
		glVertex3f( locX-.02  , locY-.02 , locZ );
		glVertex3f( locX  , locY , locZ+height );

	glEnd( );

	glBegin( GL_TRIANGLES );
		glColor3f( 0.0f, 0.0f, 1.0f );
		
		glVertex3f( locX  , locY+.02 , locZ );
		glVertex3f( locX+.02  , locY-.02 , locZ );
		glVertex3f( locX  , locY , locZ+height );

	glEnd( );



	/*GLUquadricObj *qobj;
	qobj = gluNewQuadric( );
	glRotatef( .002f , 0.0f , 1.0f , 0.0f );
	gluCylinder( qobj , .5 , .5 , height/2.0f , 50, 50 );
	*/

/*	// right leg
	glBegin( GL_TRIANGLES );
		glColor3f( 0.0f , 0.0f , 1.0f );
		glVertex3f( locX     , locY    , locZ + height/2.0f );
		glVertex3f( locX+.04f , locY    , locZ + height/2.0f );
		glVertex3f( locX+.02f , locY    , locZ );
	glEnd( );

	// left leg
	glBegin( GL_TRIANGLES );
		glColor3f( 0.0f , 1.0f , 0.0f );
		glVertex3f( locX     , locY    , locZ + height/2.0f );
		glVertex3f( locX-.04f , locY    , locZ + height/2.0f );
		glVertex3f( locX-.02f , locY    , locZ );
	glEnd( );
*/

	//std::cout << "player : " << locX << " " << locY << " " << locZ << "\n";


}

/*
void character::setLocation( float x , float y , float z )
{
	locX = x;
	locY = y;
	//locZ = z;
}
*/





/*
void character::increaseX( float newVal )
{
	locX += newVal ; 
}

void character::decreaseX( float newVal )
{
	locX -= newVal ;
}

void character::increaseY( float newVal )
{
	locY += newVal ;
}

void character::decreaseY( float newVal )
{
	locY -= newVal ; 
}

void character::increaseZ( float newVal )
{
	locZ += newVal ;
}

void character::decreaseZ( float newVal )
{
	locZ -= newVal ;
}
*/
