#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <GL/glx.h>

#include <GL/freeglut.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

//#include <iostream.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
//#include <conio.h>
#include <ncurses.h>

#include <X11/Xlib.h>

#include "character/character.h"

class window
{
	public:
		void draw( int , char** );
		//void draw( );

	protected:


	private:
		static void display( );
		static void keyboard( unsigned char , int , int );
		void init( );
		character player;

};
