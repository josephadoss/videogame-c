//#include "game.h"

#include "globals.h"

#include <iostream>
#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "SDL/SDL.h"
#include "SDL/SDL_opengl.h"

#include "character/character.h"
#include "land/landManager.h"


int main(int argc, char** argv)
{

	std::cout << "main( int argc , char** argv) \n";
    	//game *g = new game( argc , argv );

	/*SDL_Surface *hello = NULL;
	SDL_Surface *screen = NULL;

	SDL_Init( SDL_INIT_EVERYTHING );
	screen = SDL_SetVideoMode( 640 , 480 , 32 , SDL_SWSURFACE );
	hello = SDL_LoadBMP( "BLU.BMP" );
	
	SDL_BlitSurface( hello , NULL , screen , NULL );
	SDL_Flip( screen );
	SDL_Delay( 2000 );
	*/

	SDL_Event event;

	bool upPress = false;
	bool downPress = false;
	bool leftPress = false;
	bool rightPress = false;

	bool zIncrease = false;
	bool zDecrease = false;
	bool yIncrease = false;
	bool yDecrease = false;
	bool xIncrease = false;
	bool xDecrease = false;

	float fltZZoom = .001;
	float fltYZoom = fltZZoom;
	float fltXZoom = fltZZoom;

	SDL_Init( SDL_INIT_EVERYTHING );
	SDL_SetVideoMode( 640 , 480 , 32 , SDL_OPENGL | SDL_DOUBLEBUF );
	SDL_EnableUNICODE( SDL_TRUE );

	//glMatrixMode( GL_PROJECTION );
	//glLoadIdentity( );
	//glClearColor( 0.0f , 0.0f , 0.0f , 1.0f );


	character player;
	landManager land = landManager( );

	do
	{
		// Get Input

		if( SDL_PollEvent( &event ) )
		{

			if( event.type == SDL_KEYDOWN )
			{

				switch( event.key.keysym.sym )//switch( ch )
				{
					case SDLK_UP : //case 72:
						std::cout << "Up pressed\n" ;
						upPress = true ; 
						break;
					case SDLK_LEFT : //case 75:
						std::cout << "Left pressed\n" ;
						leftPress = true ;
						break;
					case SDLK_RIGHT : //case 77:
						std::cout << "Right pressed\n" ;
						rightPress = true ;
						break;
					case SDLK_DOWN : //case 80:
						std::cout << "Down pressed\n" ;
						downPress = true ; 
						break;
					
					case SDLK_z :
						std::cout << "Z up \n";
						zIncrease = true;
						break;
					case SDLK_x :
						std::cout << "Z down \n";
						zDecrease = true;
						break;
					case SDLK_a :
						std::cout << "X up \n";
						xIncrease = true;
						break;
					case SDLK_s :
						std::cout << "X down \n";
						xDecrease = true;
						break;
					case SDLK_q :
						std::cout << "Y up \n";
						yIncrease = true;
						break;
					case SDLK_w :
						std::cout << "Y down \n";
						yDecrease = true;
						break;
					default:
							
						break;
				}

			}

			if( event.type == SDL_KEYUP )
			{
				switch( event.key.keysym.sym )
				{
					case SDLK_UP :
						std::cout << "Up released\n" ;
						upPress = false ;
						break;
					case SDLK_LEFT :
						std::cout << "Left released\n" ;
						leftPress = false ;
						break;
					case SDLK_RIGHT :
						std::cout << "Right released\n" ;
						rightPress = false ;
						break;
					case SDLK_DOWN :
						std::cout << "Down released\n" ;
						downPress = false ;
						break;

					case SDLK_z :
						std::cout << "Z up released \n";
						zIncrease = false;
						break;
					case SDLK_x :
						std::cout << "Z down released \n";
						zDecrease = false;
						break;
					case SDLK_a :
						std::cout << "X up released \n";
						xIncrease = false;
						break;
					case SDLK_s :
						std::cout << "X down released \n";
						xDecrease = false;
						break;
					case SDLK_q :
						std::cout << "Y up released \n";
						yIncrease = false;
						break;
					case SDLK_w :
						std::cout << "Y down released \n";
						yDecrease = false;
						break;

					default :
						break;
				}
			}
			
			if( event.type == SDL_QUIT )
			{
				break;
			}

		}

		if( upPress && leftPress )
		{
			land.stepUp( );
			land.stepLeft( );
		}else if( upPress && rightPress )
		{
			land.stepUp( );
			land.stepRight( );
		}else if( downPress && leftPress )
		{
			land.stepDown( );
			land.stepLeft( );
		}else if( downPress && rightPress )
		{
			land.stepDown( );
			land.stepRight( );
		}else if( upPress )
		{
			land.stepUp( );
		}else if( downPress )
		{
			land.stepDown( );
		}else if( leftPress )
		{
			land.stepLeft( );
		}else if( rightPress )
		{
			land.stepRight( );
		}


		// Update Objects
		//glClear( GL_COLOR_BUFFER_BIT );
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		//            fild of view angle , aspect ratio , znear , zfar 
		//gluPerspective(0.0, 1.0, -1000.0, 1000.0);
		//glOrtho( left , right, bottom, top, zNear, zFar );
		glOrtho( -10.0 , 10.0 , -10.0 , 10.0 , -10.0 , 100.0 );
		//glDepthRange( -100.0 , 100.0 );
		//glDepthRange( 100.00 , -100.00 );



		glMatrixMode( GL_MODELVIEW );
		//glMatrixMode(GL_PROJECTION);
		glLoadIdentity( );


		// Player needs to stay at 0,0,0 
		// Make the rest of the world move around it
		/*gluLookAt( 0.0 , 0.0 , 3.0 ,
			   0.0 , 0.0 , 0.0 , //player.getX( ) , player.getY( ) , player.getZ( ) ,
			   0 , 1 , 0 );*/
		glRotatef( 45.0f , 1.0f , 0.0f , 0.0f );


		glClearColor( 0.0 , 0.0 , 0.0 , 1.0 );
		glClear( GL_COLOR_BUFFER_BIT );

		land.render( );
		player.render( );



		GLenum errCode;
		const GLubyte *errString;

		if ((errCode = glGetError()) != GL_NO_ERROR) 
		{
			errString = gluErrorString(errCode);
		   	fprintf (stderr, "OpenGL Error: %s\n", errString);
		}


		
		// Display
		
		//glFlush( );
		//glEnable( GL_LIGHTING );
		//glEnable( GL_LIGHT0 );
		glEnable( GL_DEPTH_TEST );
		//glDepthMask( GL_TRUE );
		//glDepthFunc( GL_LEQUAL );
		//glDepthRange( -1000 , 1000 );

		SDL_GL_SwapBuffers( );

	}
	while( true ) ;

	//SDL_FreeSurface( hello );
	SDL_Quit( );
	

	return 0;
}


