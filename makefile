CC := g++

SRC := src/
CHARACTER := $(SRC)character/
LAND := $(SRC)land/
STORY := $(SRC)story/
PLOT := $(LAND)plots/
PANEL := $(LAND)panels/




all: game


clean:
	rm -rf $(SRC)*o $(CHARACTER)*o $(LAND)*o $(STORY)*o *o game


game: main.o character.o plot.o panel.o land.o landManager.o
	$(CC) main.o character.o land.o plot.o panel.o landManager.o -o game -lSDL -lGL -lncurses -lGLU


main.o: $(SRC)main.cpp
	$(CC) -c $(SRC)main.cpp


character.o : $(CHARACTER)character.cpp
	$(CC) -c $(CHARACTER)character.cpp

land.o : $(LAND)land.cpp
	$(CC) -c $(LAND)land.cpp

plot.o : $(PLOT)plot.cpp
	$(CC) -c $(PLOT)plot.cpp

panel.o : $(PANEL)panel.cpp
	$(CC) -c $(PANEL)panel.cpp

landManager.o : $(LAND)landManager.cpp
	$(CC) -c $(LAND)landManager.cpp

#game: main.o window.o mainmenu.o game.o character.o
#	$(CC) main.o window.o mainmenu.o game.o character.o -o game -lGL -lncurses -lGLU



#game.o : $(SRC)game.cpp
#	$(CC) -c $(SRC)game.cpp

#main.o: $(SRC)main.cpp
#	$(CC) -c $(SRC)main.cpp

#mainmenu.o: $(SRC)mainmenu.cpp
#	$(CC) -c $(SRC)mainmenu.cpp

#window.o: $(SRC)window.cpp
#	$(CC) -c $(SRC)window.cpp 







